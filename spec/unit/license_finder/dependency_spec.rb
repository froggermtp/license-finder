# frozen_string_literal: true

require 'spec_helper'

RSpec.describe LicenseFinder::Dependency do
  describe "#licenses" do
    subject { described_class.new('bundler', 'example', '0.1.0', { spec_licenses: declared_licenses }).licenses }

    context "when the declared licenses is a single known license" do
      let(:declared_licenses) { ['MIT'] }

      specify { expect(subject.map(&:short_name)).to match_array(['MIT']) }
    end

    context "when the declared licenses is a many known licenses" do
      let(:declared_licenses) { ['MIT', 'Apache-2.0'] }

      specify { expect(subject.map(&:short_name)).to match_array(%w[MIT Apache2]) }
    end

    context "when the declared licenses is x OR y" do
      let(:declared_licenses) { ['MIT OR Apache-2.0'] }

      specify { expect(subject.map(&:short_name)).to match_array(%w[MIT Apache2]) }
    end

    context "when the declared licenses is x OR y OR z" do
      let(:declared_licenses) { ['(BSD-2-Clause OR MIT OR Apache-2.0)'] }

      specify { expect(subject.map(&:short_name)).to match_array(%w[SimplifiedBSD MIT Apache2]) }
    end

    context "when the declared licenses is x AND y" do
      let(:declared_licenses) { ['MIT AND Apache-2.0'] }

      specify { expect(subject.map(&:short_name)).to match_array(%w[MIT Apache2]) }
    end

    context "when the declared licenses is x WITH exception" do
      let(:declared_licenses) { ['Apache-2.0 WITH LLVM-exception'] }

      specify { expect(subject.map(&:short_name)).to match_array(%w[Apache2 LLVM-exception]) }
    end
  end
end
