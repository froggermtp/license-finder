# frozen_string_literal: true

require_relative '../../lib/license/management/version.rb'
name "license-management"
maintainer "GitLab B.V."
homepage "https://gitlab.com/gitlab-org/security-products/analyzers/license-finder"
license_file "LICENSE"

install_dir "#{default_root}/gitlab"
build_version License::Management::VERSION
build_iteration 1

dependency "license_management"
package_scripts_path Pathname.pwd.join("config/scripts/license_management")

package :deb do
  compression_level 9
  compression_type :xz
end
